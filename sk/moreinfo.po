# Slovak messages for release-notes.
# Copyright (C) 2009 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2019-07-06 10:20+0200\n"
"PO-Revision-Date: 2017-06-16 15:21+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "sk"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Ďalšie informácie o Debiane"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Ďalšie čítanie"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Okrem týchto Poznámok k vydaniu a Inštalačnej príručky ďalšiu dokumentáciu o "
"systéme Debian nájdete v rámci Dokumentačného projektu Debian (DDP), ktorého "
"cieľom je tvoriť kvalitnú dokumentáciu pre používateľov a vývojárov Debianu. "
"Medzi dostupnú dokumentáciu patrí Debian Reference, Debian New Maintainers "
"Guide, Debian FAQ a mnohé ďalšie. Podrobnosti o existujúcich zdrojoch "
"nájdete na <ulink url=\"&url-ddp;\">webstránke DDP</ulink> a na <ulink url="
"\"&url-wiki;\">Debian Wiki</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Dokumentácia jednotlivých balíkov sa inštaluje do adresára <filename>/usr/"
"share/doc/<replaceable>balík</replaceable></filename>. Sem patria informácie "
"o autorských právach, podrobnosti o balíku špecifické pre Debian a všetka "
"dokumentácia pochádzajúca od pôvodných autorov."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Ako získať pomoc"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Existuje mnoho spôsobov ako získať pomoc, rady a podporu pri používaní "
"Debianu, no mali by ste ich zvážiť až potom, čo ste pri skúmaní problému "
"prehľadali všetku dostupnú dokumentáciu. Tento oddiel poskytuje krátky úvod "
"k tým informačným kanálom, ktoré môžu pomôcť novým používateľom Debianu."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Konferencie"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Konferencie elektronickej pošty, ktoré najviac zaujímajú používateľov "
"Debianu, sú debian-user (po anglicky) a ostatné konferencie debian-user-"
"<replaceable>jazyk</replaceable> (v ostatných jazykoch). Informácie o týchto "
"konferenciách a ako sa do nich prihlásiť popisuje <ulink url=\"&url-debian-"
"list-archives;\"></ulink>. Rešpektujte prosím štandardnú etiketu "
"elektronickej komunikácie a konferencií a než pošlete svoju otázku, "
"pohľadajte najskôr v archívoch konferencie či sa ju už niekto pýtal."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian má na IRC sieti OFTC kanál určený na podporu a pomoc používateľom "
"Debianu. Kanál nájdete pod menom <literal>#debian</literal> na serveri irc."
"debian.org."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Prosím, dodržiavajte pravidlá kanála a berte ohľad na ostatných "
"používateľov. Pravidlá nájdete na <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Ďalšie informácie o OFTC nájdete na jeho <ulink url=\"&url-irc-host;"
"\">webovej stránke</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Oznamovanie chýb"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Snažíme sa, aby sme z Debianu urobili kvalitný operačný systém - to však "
"neznamená, že balíky, ktoré poskytujeme nemajú vôbec žiadne chyby. V súlade "
"s filozofiou <quote>otvoreného vývoja</quote> Debianu a ako službu našim "
"používateľom sprístupňujeme všetky informácie o nahlásených chybách v našom "
"vlastnom Systéme sledovania chýb (BTS). BTS je možné prehliadať na adrese "
"<ulink url=\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Ak nájdete chybu v distribúcii alebo v niektorom z balíkov softvéru, ktorý "
"je jej súčasťou, oznámte ju prosím, aby ju bolo možné riadne opraviť v "
"ďalších vydaniach. Na oznámenie chyby je potrebné mať platnú emailovú "
"adresu. Vyžadujeme ju preto, aby sme mohli sledovať chyby a aby mohli "
"vývojári kontaktovať oznamovateľov, ak potrebujú podrobnejšie informácie."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Hlásenie o chybe môžete poslať pomocou programu <command>reportbug</command> "
"alebo manuálne zaslaním emailu. Viac o Systéme sledovania chýb a ako ho "
"používať sa dozviete v jeho dokumentácii (v <filename>/usr/share/doc/debian</"
"filename> ak máte nainštalovaný balík <systemitem role=\"package\">doc-"
"debian</systemitem>) alebo online na  stránke <ulink url=\"&url-bts;"
"\">Systému sledovania chýb</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Ako prispievať do Debianu"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
#, fuzzy
#| msgid ""
#| "You do not need to be an expert to contribute to Debian.  By assisting "
#| "users with problems on the various user support <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> you are contributing to the community.  "
#| "Identifying (and also solving) problems related to the development of the "
#| "distribution by participating on the development <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
#| "Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
#| "ulink> and help developers track them down and fix them.  The tool "
#| "<systemitem role=\"package\">how-can-i-help</systemitem> helps you to "
#| "find suitable reported bugs to work on.  If you have a way with words "
#| "then you may want to contribute more actively by helping to write <ulink "
#| "url=\"&url-ddp-svn-info;\">documentation</ulink> or <ulink url=\"&url-"
#| "debian-i18n;\">translate</ulink> existing documentation into your own "
#| "language."
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Aby ste mohli prispievať do Debianu, nemusíte byť expert. Komunite "
"prospievate tým, že pomáhate ostatným používateľom v rôznych <ulink url="
"\"&url-debian-list-archives;\">konferenciách</ulink> používateľskej podpory. "
"Identifikáciou (a tiež riešením) problémov týkajúcich sa vývoja distribúcie, "
"ak sa zapojíte do vývojárskych <ulink url=\"&url-debian-list-archives;"
"\">konferencií</ulink> môžete tiež veľmi pomôcť. Aby sa udržala vysoká "
"kvalita distribúcie Debian, <ulink url=\"&url-bts;\">posielajte hlásenia o "
"chybách</ulink> a pomáhajte vývojárom sledovať a opraviť ich. Nástroj "
"<systemitem role=\"package\">how-can-i-help</systemitem> (ako môžem pomôcť) "
"vám pomôže nájsť vhodné nahlásené chyby, na ktorých môžete pracovať. Ak ste "
"šikovný v písaní, môžete aktívnejšie prispieť tým, že pomôžete písať <ulink "
"url=\"&url-ddp;\">dokumentáciu</ulink> alebo <ulink url=\"&url-debian-i18n;"
"\">prekladať</ulink> existujúcu dokumentáciu do svojho jazyka."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Ak chcete venovať viac času, môžete v rámci Debianu spravovať časť kolekcie "
"slobodného softvéru. Obzvlášť užitočné je, ak ľudia prevezmú alebo začnú "
"spravovať veci, ktoré si niekto vyžiadal zaradiť do Debianu. Podrobnosti o "
"tomto nájdete v <ulink url=\"&url-wnpp;\">databáze balíkov, ktoré potrebujú "
"pomoc a perspektívnych balíkov</ulink>. Ak vás zaujímajú konkrétne skupiny, "
"môže vás baviť účasť v niektorom z podprojektov Debianu, kam patria okrem "
"iného porty na rôzne architektúry a <ulink url=\"&url-debian-blends;"
"\">Debian Pure Blends</ulink> pre špecifické skupiny používateľov."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"V každom prípade, ak sa akýmkoľvek spôsobom podieľate na komunite slobodného "
"softvéru, či ako používateľ, programátor, tvorca dokumentácie alebo "
"prekladateľ, pomáhate tým hnutiu slobodného softvéru. Prispievanie je veľmi "
"užitočné a často aj zábavné, umožňuje vám spoznať nových ľudí a dáva vám "
"ťažko popísateľný hrejivý pocit."
